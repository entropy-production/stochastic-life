extern crate conrod_core;
extern crate glium;

use crate::stolif::Stolif;
use std::sync::{Arc, Mutex};

use futures_util::future::{FusedFuture, FutureExt, TryFutureExt};
use tokio::runtime::Runtime;
use tokio::sync::oneshot::Receiver;
use tokio::sync::{mpsc, RwLock};

mod stolif;
mod ui;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let stolif_lock = Arc::new(Mutex::new(Stolif::new(60)));

    ui::ui_code(stolif_lock);

    return Ok(());
}
